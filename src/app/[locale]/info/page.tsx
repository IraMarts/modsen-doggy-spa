'use client';

import { getDictionary } from '@/locale';
import { type LocaleParamsProps } from '@/types';

const Info = ({ params: { locale } }: LocaleParamsProps) => {
  const { info } = getDictionary(locale);

  return <main>{info.title}</main>;
};

export default Info;
