'use client';

import { useLazyQuery } from '@apollo/client';
import { useQuery } from '@apollo/experimental-nextjs-app-support/ssr';
import { Button } from '@marts/modsen-doggy-ui-kit';

import { GET_DOGS_BY_ENERGY } from '@/graphql/queries/getDogByEnergy';
import { GET_DOGS } from '@/graphql/queries/getDogs';
import { getDictionary } from '@/locale/getDictionary';
import { type LocaleParamsProps } from '@/types';

const Home = ({ params: { locale } }: LocaleParamsProps) => {
  const { test } = getDictionary(locale);

  const { data: dogs } = useQuery(GET_DOGS, {
    variables: {
      value: 'Carolina Dog',
    },
  });

  const [getDogByEnergy, { data }] = useLazyQuery(GET_DOGS_BY_ENERGY, {
    variables: {
      energy: 2,
    },
  });

  const handleClick = (): void => {
    void getDogByEnergy();
  };

  return (
    <main>
      {test}
      <div>{JSON.stringify({ dogs })}</div>
      <Button onClick={handleClick}>My pink button</Button>
      {data && <div>{JSON.stringify({ data })}</div>}
    </main>
  );
};

export default Home;
