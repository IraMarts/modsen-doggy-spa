'use client';
import { type PropsWithChildren } from 'react';

import { ApolloWrapper } from '@/components';

const Layout = ({ children }: PropsWithChildren) => {
  return <ApolloWrapper>{children}</ApolloWrapper>;
};

export default Layout;
