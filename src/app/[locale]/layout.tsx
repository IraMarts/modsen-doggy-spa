import { type PropsWithChildren } from 'react';

import { Cormorant } from 'next/font/google';

import { Footer, Header } from '@/components';
import { getDictionary, i18n } from '@/locale';
import { GlobalStyles } from '@/styles/global-styles';
import { type LocaleParamsProps } from '@/types';

const cormorant = Cormorant({
  weight: ['400', '700'],
  subsets: ['latin', 'cyrillic'],
});

export const metadata = {
  title: `Modsen Doggy SPA`,
  description: 'Blog about cute doggs',
  icons: { icon: ['/favicon.ico'], apple: ['/favicon.ico'] },
};

export async function generateStaticParams() {
  return i18n.locales.map((locale) => ({ locale }));
}

const RootLayout = ({
  children,
  params: { locale },
}: PropsWithChildren<LocaleParamsProps>) => {
  const { navLinks, footer, form, headerPromo } = getDictionary(locale);

  return (
    <html lang={locale}>
      <body className={cormorant.className}>
        <GlobalStyles />
        <Header navLinks={navLinks} locale={locale} headerPromo={headerPromo} />
        {children}
        <Footer footerContent={footer} formContent={form} />
      </body>
    </html>
  );
};

export default RootLayout;
