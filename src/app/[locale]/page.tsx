import Link from 'next/link';

async function Home() {
  return (
    <div>
      <p>
        <Link href="/ssr">SSR examples are here</Link>
      </p>
    </div>
  );
}

export default Home;
