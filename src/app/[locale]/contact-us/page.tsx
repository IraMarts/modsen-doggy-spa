'use client';

import { getDictionary } from '@/locale';
import { type LocaleParamsProps } from '@/types';

const ContactUs = ({ params: { locale } }: LocaleParamsProps) => {
  const { contactUs } = getDictionary(locale);

  return <main>{contactUs.title}</main>;
};

export default ContactUs;
