'use client';

import { getDictionary } from '@/locale';
import { type LocaleParamsProps } from '@/types';

const Blog = ({ params: { locale } }: LocaleParamsProps) => {
  const { blog } = getDictionary(locale);

  return <main>{blog.title}</main>;
};

export default Blog;
