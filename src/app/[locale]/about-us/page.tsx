'use client';

import { getDictionary } from '@/locale';
import { type LocaleParamsProps } from '@/types';

const AboutUs = ({ params: { locale } }: LocaleParamsProps) => {
  const { aboutUs } = getDictionary(locale);

  return <main>{aboutUs.title}</main>;
};

export default AboutUs;
