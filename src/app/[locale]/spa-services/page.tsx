'use client';

import { getDictionary } from '@/locale';
import { type LocaleParamsProps } from '@/types';

const SpaServices = ({ params: { locale } }: LocaleParamsProps) => {
  const { spaServices } = getDictionary(locale);

  return <main>{spaServices.title}</main>;
};

export default SpaServices;
