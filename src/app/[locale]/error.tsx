'use client';

import { Button } from '@marts/modsen-doggy-ui-kit';

import { type ErrorPageProps } from '@/types';

const ErrorScreen = ({ error }: ErrorPageProps) => {
  const handlePageReload = () => {
    window.location.reload();
  };

  return (
    <div>
      <div>
        <h2>Something went wrong! Error has occured</h2>
        <h3>{error.name}</h3>
        <p>{error.digest}</p>
        <h3>{error.message}</h3>

        <Button onClick={handlePageReload}>Reload the page</Button>
      </div>
    </div>
  );
};

export default ErrorScreen;
