'use client';

import { getDictionary } from '@/locale';
import { type LocaleParamsProps } from '@/types';

const BookAppointment = ({ params: { locale } }: LocaleParamsProps) => {
  const { booking } = getDictionary(locale);

  return <main>{booking.title}</main>;
};

export default BookAppointment;
