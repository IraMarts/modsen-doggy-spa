import { ApolloServer } from '@apollo/server';
import { ApolloServerPluginLandingPageGraphQLPlayground } from '@apollo/server-plugin-landing-page-graphql-playground';
import { startServerAndCreateNextHandler } from '@as-integrations/next';
import { type NextRequest } from 'next/server';

import resolvers from '@/graphql/resolvers';
import typeDefs from '@/graphql/schemas';

export interface MyServerContext {
  'X-Api-Key'?: string;
}

const apolloServer = new ApolloServer<MyServerContext>({
  typeDefs,
  resolvers,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
});

const handler = startServerAndCreateNextHandler<NextRequest>(apolloServer, {
  context: async (req, res) => ({
    req,
    res,
    headers: {
      'X-Api-Key': process.env.NEXT_PUBLIC_API_KEY,
    },
  }),
});

export async function GET(request: NextRequest): Promise<Response> {
  return await handler(request);
}
export async function POST(request: NextRequest): Promise<Response> {
  return await handler(request);
}
