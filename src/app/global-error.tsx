'use client';

import { Button } from '@marts/modsen-doggy-ui-kit';

import { type ErrorPageProps } from '@/types';

const GlobalError = ({ error }: ErrorPageProps) => {
  const handlePageReload = () => {
    window.location.reload();
  };

  return (
    <html lang="en">
      <body>
        <div>
          <h1>Something went wrong! Global Error</h1>
          <h3>{error.name}</h3>
          <p>{error.digest}</p>
          <h3>{error.message}</h3>

          <Button onClick={handlePageReload}>Reload the page</Button>
        </div>
      </body>
    </html>
  );
};

export default GlobalError;
