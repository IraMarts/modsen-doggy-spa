export { getDictionary, type DictionaryType } from './getDictionary';
export { i18n, type Locale } from './i18n.config';
