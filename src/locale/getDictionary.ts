import en from './en.json';
import type { Locale } from './i18n.config';
import ru from './ru.json';

const dictionaries = {
  en,
  ru,
};

export const getDictionary = (locale: Locale) =>
  dictionaries[locale] ?? dictionaries.en;

export type DictionaryType = ReturnType<typeof getDictionary>;
