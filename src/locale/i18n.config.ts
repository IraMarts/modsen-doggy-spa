export const i18n = {
  defaultLocale: 'en',
  locales: ['en', 'ru'],
} as const;

export type Locale = I18nConfig['locales'][number];
type I18nConfig = typeof i18n;
