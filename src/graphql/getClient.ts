import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import { registerApolloClient } from '@apollo/experimental-nextjs-app-support/rsc';

// client for use in RSC
export const { getClient } = registerApolloClient(() => {
  return new ApolloClient({
    link: new HttpLink({
      uri:
        process.env.NEXT_PUBLIC_URL_SERVER_GRAPHQL ??
        `https://${process.env.NEXT_PUBLIC_VERCEL_URL}/api/graphql`,
    }),
    cache: new InMemoryCache(),
  });
});
