import { gql } from 'graphql-tag';

const typeDefs = gql`
  type Query {
    dogs(value: String): [Dog]
    dogsByEnergy(energy: Int): [Dog]
  }

  type Dog {
    name: String
    energy: Int
    minWeight: Int
    barking: Int
    imageLink: String
  }
`;

export default typeDefs;
