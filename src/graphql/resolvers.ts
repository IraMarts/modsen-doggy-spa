import { type Dog } from '@/types';

const resolvers = {
  Query: {
    dogs: async (
      _: undefined,
      { value }: { value: string },
      context?: RequestInit,
    ) => {
      try {
        const response = await fetch(
          `${process.env.NEXT_PUBLIC_URL_API}?name=${value}` ||
            'golden retriever',
          context,
        );
        const data = await response.json();

        console.log(data);

        return data.map(
          ({ name, energy, minWeight, barking, imageLink }: Dog) => {
            return {
              id: Math.random(),
              name,
              energy,
              minWeight,
              barking,
              imageLink,
            };
          },
        );
      } catch (error) {
        throw new Error('Something went wrong');
      }
    },
    dogsByEnergy: async (
      _: undefined,
      { energy }: { energy: number },
      context?: RequestInit,
    ) => {
      try {
        const response = await fetch(
          `${process.env.NEXT_PUBLIC_URL_API}?energy=${energy}` || '',
          context,
        );
        const data = await response.json();

        console.log(data);

        return data.map(
          ({ name, energy, minWeight, barking, imageLink }: Dog) => {
            return {
              id: Math.random(),
              name,
              energy,
              minWeight,
              barking,
              imageLink,
            };
          },
        );
      } catch (error) {
        console.log(error);
        throw new Error('Something went wrong');
      }
    },
  },
};

export default resolvers;
