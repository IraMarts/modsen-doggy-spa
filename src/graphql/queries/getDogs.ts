import { gql } from '@apollo/client';

export const GET_DOGS = gql`
  query Dogs($value: String) {
    dogs(value: $value) {
      name
      energy
      minWeight
      barking
      imageLink
    }
  }
`;
