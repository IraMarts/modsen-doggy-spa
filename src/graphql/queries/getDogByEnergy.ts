import { gql, type TypedDocumentNode } from '@apollo/client';

import { type Dog } from '@/types';

export const GET_DOGS_BY_ENERGY: TypedDocumentNode<Dog, { energy: number }> =
  gql`
    query getDogByEnergy($energy: Int) {
      dogsByEnergy(energy: $energy) {
        name
        energy
        minWeight
        barking
        imageLink
      }
    }
  `;
