import { type ILocale, type INavLinks } from '@/types';

type Link = Record<'id' | 'href' | 'title', string>;
type ParamsType = ILocale & INavLinks;

type GetNavLinksType = (params: ParamsType) => Link[];

export const getNavLinks: GetNavLinksType = ({ navLinks, locale }) => {
  const { home, aboutUs, contactUs, blog, bookAppointment, info, spaServices } =
    navLinks;
  const links = [
    {
      id: `${locale}`,
      title: home,
      href: '/',
    },
    {
      id: 'info',
      title: info,
      href: '/info',
    },
    {
      id: 'spa-services',
      title: spaServices,
      href: '/spa-services',
    },
    {
      id: 'book-appointment',
      title: bookAppointment,
      href: '/book-appointment',
    },
    {
      id: 'blog',
      title: blog,
      href: '/blog',
    },
    {
      id: 'about-us',
      title: aboutUs,
      href: '/about-us',
    },
    {
      id: 'contact-us',
      title: contactUs,
      href: '/contact-us',
    },
  ];

  return links;
};
