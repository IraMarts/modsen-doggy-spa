import { type DictionaryType, type Locale } from '@/locale';

export interface Dog {
  name: string;
  energy: number;
  minWeight: number;
  barking: number;
  imageLink: string;
}

export interface ILocale {
  locale: Locale;
}

export interface LocaleParamsProps {
  params: ILocale;
}

export interface ErrorPageProps {
  error: Error & { digest?: string };
  reset: () => void;
}

export interface INavLinks {
  navLinks: DictionaryType['navLinks'];
}
