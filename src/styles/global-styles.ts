'use client';
import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
*:root {
  --accent-background: #DFB2A9;
  --primary-background: #f3ded7;
  --primary-text: #4c4c4b;
  --input-border: #e0e0e0;
}


  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }

  
  * {
    padding: 0;
    margin: 0;
  }

  html,
  body,
  #__next {
    height: 100%;
    font-family: "Cormorant";
  }


  body {
    line-height: 1.7;
    -webkit-font-smoothing: antialiased;
  }
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    line-height: 1.2;
    color: var(--primary-text);
  }

 
  img,
  picture,
  video,
  canvas,
  svg {
    display: block;
    max-width: 100%;
  }


  input,
  button,
  textarea,
  select {
    font: inherit;
  }

  a {
  text-decoration: none;
  }

  ul {
    list-style-type: none
  }
  * {
    overflow-wrap: break-word;
  }


`;
