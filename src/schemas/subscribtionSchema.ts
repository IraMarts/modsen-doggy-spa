import { object, string } from 'yup';

import { EMAIL_NOT_VALID, THE_FIELD_IS_REQUIRED } from '@/constants';

export const emailSchema = object().shape({
  email: string().email(EMAIL_NOT_VALID).required(THE_FIELD_IS_REQUIRED),
});
