import { useCallback, useState, type ChangeEvent } from 'react';

import { send } from '@emailjs/browser';

import { ENV_NOT_VALID, SUCCESSFUL_REQUEST, UNKNOWN_ERROR } from '@/constants';
import { emailSchema } from '@/schemas';

export const useSubscribeToEmails = () => {
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const handleEmailChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setEmail(event?.target.value);
    },
    [],
  );

  const handleSubscribeToEmails = useCallback(
    async (event: ChangeEvent<HTMLFormElement>) => {
      event.preventDefault();

      try {
        setIsLoading(true);

        if (
          !(
            process.env.NEXT_PUBLIC_EMAILJS_SERVICE_ID &&
            process.env.NEXT_PUBLIC_EMAILJS_SUBSCRIBTION_TEMPLATE_ID &&
            process.env.NEXT_PUBLIC_EMAILJS_PUBLIC_KEY
          )
        ) {
          throw Error(ENV_NOT_VALID);
        }

        await emailSchema.validate({
          email,
        });

        await send(
          process.env.NEXT_PUBLIC_EMAILJS_SERVICE_ID,
          process.env.NEXT_PUBLIC_EMAILJS_SUBSCRIBTION_TEMPLATE_ID,
          { email },
          process.env.NEXT_PUBLIC_EMAILJS_PUBLIC_KEY,
        );

        alert(SUCCESSFUL_REQUEST);
      } catch (error: unknown) {
        if (error instanceof Error) {
          alert(error.message);
          console.log(error.message);
        } else {
          console.log(UNKNOWN_ERROR);
        }
      } finally {
        setIsLoading(false);
      }
    },
    [email],
  );

  return {
    email,
    isLoading,
    handleEmailChange,
    handleSubscribeToEmails,
  };
};
