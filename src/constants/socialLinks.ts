export const SOCIAL_LINKS = [
  {
    title: 'instagram',
    href: 'https://www.instagram.com/',
    path: '/assets/instagram.svg',
  },
  {
    title: 'facebook',
    href: 'https://www.facebook.com/',
    path: '/assets/facebook.svg',
  },
  {
    title: 'pinterest',
    href: 'https://www.pinterest.com/',
    path: '/assets/pinterest.svg',
  },
  {
    title: 'twitter',
    href: 'https://www.twitter.com/',
    path: '/assets/twitter.svg',
  },
  {
    title: 'snapchat',
    href: 'https://www.snapchat.com/',
    path: '/assets/snapchat.svg',
  },
];
