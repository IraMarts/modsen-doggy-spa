export const UNKNOWN_ERROR = 'Uknown Error has occured. Please try later';
export const SUCCESSFUL_REQUEST = 'Your request was sent successfully';
export const EMAIL_NOT_VALID = 'Please enter a valid email address';
export const THE_FIELD_IS_REQUIRED = 'This field is required';
export const ENV_NOT_VALID = 'Something is wrong with your settings in .env';
