'use client';
import { css, styled } from 'styled-components';

export const CustomNavLink = styled.a<{
  $isActive?: boolean;
}>`
  cursor: pointer;
  color: #4c4c4b;
  font-size: 26px;
  font-weight: 700;
  position: relative;
  padding: 8px;

  &:hover {
    transition: all 0.5s ease-out;
    color: #dfb2a9;
  }

  ${({ $isActive }) =>
    $isActive &&
    css`
      &:after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        background-color: #e89b93;
        height: 6px;
      }
    `}
`;
