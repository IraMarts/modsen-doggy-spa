'use client';

import { type PropsWithChildren } from 'react';

import {
  ApolloLink,
  HttpLink,
  type NormalizedCacheObject,
} from '@apollo/client';
import {
  ApolloNextAppProvider,
  NextSSRApolloClient,
  NextSSRInMemoryCache,
  SSRMultipartLink,
} from '@apollo/experimental-nextjs-app-support/ssr';

// for ssr
export const ApolloWrapper = ({ children }: PropsWithChildren): JSX.Element => {
  return (
    <ApolloNextAppProvider makeClient={makeClient}>
      {children}
    </ApolloNextAppProvider>
  );
};

function makeClient(): NextSSRApolloClient<NormalizedCacheObject> {
  const httpLink = new HttpLink({
    uri:
      process.env.NEXT_PUBLIC_URL_SERVER_GRAPHQL ??
      `https://${process.env.NEXT_PUBLIC_VERCEL_URL}/api/graphql`,
    fetchOptions: { cache: 'no-store' },
  });

  return new NextSSRApolloClient({
    cache: new NextSSRInMemoryCache(),
    link:
      typeof window === 'undefined'
        ? ApolloLink.from([
            new SSRMultipartLink({
              stripDefer: true,
            }),
            httpLink,
          ])
        : httpLink,
  });
}
