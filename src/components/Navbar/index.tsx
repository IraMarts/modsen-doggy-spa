'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';

import { CustomNavLink } from '../CustomNavLink';

import { NavContainer } from './styled';

import { type Locale } from '@/locale';
import { type INavLinks } from '@/types';
import { getNavLinks } from '@/utils/getLinks';

interface NavbarProps extends INavLinks {
  locale: Locale;
}

export const Navbar = ({ navLinks, locale }: NavbarProps) => {
  const pathName = usePathname();
  const currentPath = pathName.split('/').pop();
  const links = getNavLinks({ navLinks, locale });

  return (
    <NavContainer>
      {links.map(({ id, title, href }) => {
        const isActive = currentPath === id;

        return (
          <Link key={id} href={href} passHref legacyBehavior>
            <CustomNavLink $isActive={isActive}>{title}</CustomNavLink>
          </Link>
        );
      })}
    </NavContainer>
  );
};
