import { css, styled } from 'styled-components';

export const FlexContainer = styled.div<{
  $direction?: string;
  $gap?: string;
  $justifyContent?: string;
  $maxWidth?: string;
}>`
  display: flex;
  gap: ${({ $gap }) => $gap ?? 0};
  flex-direction: ${({ $direction }) =>
    $direction === 'column' ? 'column' : 'row'};
  justify-content: ${({ $justifyContent }) => $justifyContent ?? 'flex-start'};
  ${({ $maxWidth }) => css`
    max-width: ${$maxWidth};
  `};
`;
