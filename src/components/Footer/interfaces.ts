import { type DictionaryType } from '@/locale';

export interface IFooterProps {
  footerContent: DictionaryType['footer'];
  formContent: DictionaryType['form'];
}
