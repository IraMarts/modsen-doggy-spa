import { styled } from 'styled-components';

import { CustomNavLink } from '../CustomNavLink';

export const Wrapper = styled.footer`
  display: flex;
  flex-direction: column;
  height: 12px;
  border-top: 12px var(--accent-background) solid;
`;

export const NavContainer = styled.div`
  background-color: var(--primary-background);
  display: flex;
  justify-content: space-around;
  padding: 54px 26px 84px;
`;

export const InfoContainer = styled.div`
  background-color: var(--accent-background);
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 4px 96px;
`;

export const InfoLink = styled(CustomNavLink)`
  font-size: 22px;
  font-weight: 400;
  padding: 7px;

  &:hover {
    color: var(--pimary-text);
    font-weight: 700;
  }
`;

export const FooterLink = styled(InfoLink)`
  padding: 7px 0;
`;

export const LinksList = styled.ul`
  margin-top: 23px;
`;
