'use client';
import Image from 'next/image';
import Link from 'next/link';

import { ContactForm } from '../ContactForm';
import { FlexContainer } from '../FlexContainer';
import { Heading, Typography } from '../Typography';

import { type IFooterProps } from './interfaces';
import {
  FooterLink,
  InfoContainer,
  InfoLink,
  LinksList,
  NavContainer,
  Wrapper,
} from './styled';

import { SOCIAL_LINKS } from '@/constants';

export const Footer = ({ footerContent, formContent }: IFooterProps) => {
  const {
    info,
    customerService,
    subscribe,
    serviceLinks,
    socialMedia,
    navigation,
    navigationLinks,
  } = footerContent;

  const { cookiePolicy, cookieSettings, privacy, rights, security, terms } =
    info;

  const socialLinks = SOCIAL_LINKS.map(({ href, path, title }) => (
    <Link href={href} key={title} target="_blank">
      <Image src={path} alt={`Follow us in ${title}`} height={30} width={30} />
    </Link>
  ));

  return (
    <Wrapper>
      <NavContainer>
        <FlexContainer $direction="column">
          <Heading as="h3" size="32px">
            {customerService}
          </Heading>
          <LinksList>
            {Object.values(serviceLinks).map((title) => (
              <li key={title}>
                <Link href="#" passHref legacyBehavior>
                  <FooterLink>{title}</FooterLink>
                </Link>
              </li>
            ))}
          </LinksList>
        </FlexContainer>

        <FlexContainer $direction="column" $gap="72px">
          <FlexContainer $direction="column">
            <Heading as="h3" size="32px">
              {subscribe}
            </Heading>
            <ContactForm formContent={formContent} />
          </FlexContainer>

          <FlexContainer $direction="column" $gap="34px">
            <Heading as="h3" size="32px">
              {socialMedia}
            </Heading>

            <FlexContainer
              $direction="row"
              $justifyContent="space-between"
              $maxWidth="450px"
            >
              {socialLinks}
            </FlexContainer>
          </FlexContainer>
        </FlexContainer>

        <FlexContainer $direction="column">
          <Heading as="h3" size="32px">
            {navigation}
          </Heading>
          <LinksList>
            {Object.values(navigationLinks).map((title) => (
              <li key={title}>
                <Link href="#" passHref legacyBehavior>
                  <FooterLink>{title}</FooterLink>
                </Link>
              </li>
            ))}
          </LinksList>
        </FlexContainer>
      </NavContainer>

      <InfoContainer>
        <FlexContainer $gap="16px">
          <InfoLink>{cookiePolicy}</InfoLink>
          <InfoLink>{cookieSettings}</InfoLink>
        </FlexContainer>
        <Typography size="22px">{rights}</Typography>
        <FlexContainer $gap="16px">
          <InfoLink>{terms}</InfoLink>
          <InfoLink>{privacy}</InfoLink>
          <InfoLink>{security}</InfoLink>
        </FlexContainer>
      </InfoContainer>
    </Wrapper>
  );
};
