import { css, styled } from 'styled-components';

export const Typography = styled.p<{
  size: string;
}>`
  font-size: ${({ size }) => size};
`;

export const Heading = styled.h2<{ color?: string; size?: string }>`
  font-size: ${({ size }) => size ?? '70px'};
  font-weight: 700;
  ${({ color }) =>
    color &&
    css`
      color: ${color};
    `}
`;
