'use client';
import Image from 'next/image';

import { Heading } from '../Typography/styled';

import { HeaderContainer, HeaderSubContainer, InfoContainer } from './styled';

import { Navbar } from '@/components/Navbar/index';
import { type DictionaryType, type Locale } from '@/locale';
import { type INavLinks } from '@/types';

interface HeaderProps extends INavLinks {
  locale: Locale;
  headerPromo: DictionaryType['headerPromo'];
}

export const Header = ({ navLinks, locale, headerPromo }: HeaderProps) => {
  return (
    <HeaderContainer>
      <HeaderSubContainer>
        <Image
          priority
          src="/assets/logo.png"
          alt="Luxe Animal Spa"
          height={79}
          width={153}
        />
        <Navbar navLinks={navLinks} locale={locale} />
      </HeaderSubContainer>
      <InfoContainer>
        <Heading as="h3" size="26px">
          {headerPromo}
        </Heading>
      </InfoContainer>
    </HeaderContainer>
  );
};
