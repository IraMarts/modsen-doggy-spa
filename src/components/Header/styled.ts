import { styled } from 'styled-components';

export const HeaderContainer = styled.header`
  display: flex;
  flex-direction: column;
`;

export const HeaderSubContainer = styled.div`
  display: flex;
  padding: 8px 304px 8px 44px;
  gap: 152px;
`;

export const InfoContainer = styled(HeaderSubContainer)`
  justify-content: center;
  padding: 12px 86px 8px;
  background: var(--primary-background);
`;
