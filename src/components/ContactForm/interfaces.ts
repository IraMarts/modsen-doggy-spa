import { type DictionaryType } from '@/locale';

export interface IContactFormProps {
  formContent: DictionaryType['form'];
}
