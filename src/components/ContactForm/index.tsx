import { type IContactFormProps } from './interfaces';
import { InputField, SubmitButton } from './styled';

import { useSubscribeToEmails } from '@/hooks/useSubscribeToEmails';

export const ContactForm = ({ formContent }: IContactFormProps) => {
  const { email: emailPlaceholder, submit } = formContent;
  const { email, isLoading, handleEmailChange, handleSubscribeToEmails } =
    useSubscribeToEmails();

  return (
    <form onSubmit={handleSubscribeToEmails}>
      <InputField
        placeholder={emailPlaceholder}
        value={email}
        onChange={handleEmailChange}
      />
      <SubmitButton disabled={isLoading}>{submit}</SubmitButton>
    </form>
  );
};
