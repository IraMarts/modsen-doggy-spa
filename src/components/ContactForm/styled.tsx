import { Button } from '@marts/modsen-doggy-ui-kit';
import { styled } from 'styled-components';

export const InputField = styled.input`
  font-size: 22px;
  border: 1px var(--input-border) solid;
  padding: 16px 12px 18px 22px;
  border-radius: 8px;
  width: 498px;
  height: 58px;
  margin-right: 30px;
  margin-top: 34px;

  &::placeholder {
    color: #959fa8;
    font-size: 14px;
    line-height: 1.4;
  }
`;

export const SubmitButton = styled(Button)`
  width: 128px;
`;
